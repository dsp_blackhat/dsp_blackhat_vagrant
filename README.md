# dsp_blackhat_vagrant

Vagrantfile for DSP Blackhat

## Usage  
There are two types of Vagrantfile (remote or local). 
Use:  
```
 vagrant up
```

To run the box. 
If the remote download through "vagrant up " command is too slow you can use the local version: 
1. download the vagrantbox (or put into local directory the vagrant box taken out-of-band:   

```
# Download from remote the box and execute it

cd <thisrepo>/local;
wget https://vagrantcloud.com/giper45/boxes/dsp_blackhat/versions/1.0.0/providers/virtualbox.box
vagrant box add --name dsp_blackhat virtualbox.box

# OR (local approach)

mv dspbox.box <thisrepo>/local/virtualbox.box
cd <thisrepo>/local
vagrant box add --name dsp_blackhat virtualbox.box

```

Then execute vagrant up to run the box.
